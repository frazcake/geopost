package com.example.frazcake.geopost.UI;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.view.View;

import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.UserViewModel;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.FollowedUser;
import com.example.frazcake.geopost.model.MyUser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

public class UserMapsFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap map;
    private View.OnClickListener goToMyPositionListener = v -> moveToMyPosition();
    private HashMap<String,Marker> usersMarkers = new HashMap<>();
    private Marker myUserMarker=null;
    private Marker myUserLastMassageMarker=null;
    private UserViewModel userViewModel;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        if(savedInstanceState==null) {
            SupportMapFragment mapFrag = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map));
            mapFrag.getMapAsync(this);
        }
        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        getActivity().findViewById(R.id.my_position_iv).setOnClickListener(goToMyPositionListener);
    }

    public void updateMap(FollowedUser u) {
        Marker marker = usersMarkers.get(u.getUsername());
        marker.setSnippet(u.getMsg());
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(),15));
        marker.showInfoWindow();
        map.setOnMarkerClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map.getUiSettings().setMapToolbarEnabled(false);
        final Observer<List<FollowedUser>> observerUser = users -> {
            for (Marker m: usersMarkers.values()) {
                m.remove();
            }
            usersMarkers.clear();
            for (FollowedUser u: users) {
                LatLng coord = new LatLng(u.getLat(),u.getLon());
                Marker m;
                if (u.isVip())
                    m = map.addMarker(new MarkerOptions().position(coord).title(u.getUsername()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                else
                    m = map.addMarker(new MarkerOptions().position(coord).title(u.getUsername()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));m.setSnippet(u.getMsg());
                usersMarkers.put(u.getUsername(), m);
            }
        };
        userViewModel.getUsers().observe(this, observerUser);
        final Observer<MyUser> observeMyUser = myUser -> {
            LatLng myLastMassageCoord = new LatLng(myUser.getMsglat(), myUser.getMsglon());
            if(myUserLastMassageMarker!=null && (myUserLastMassageMarker.getSnippet()!=myUser.getMsg() || myUserLastMassageMarker.getPosition().latitude!=myUser.getLat() || myUserLastMassageMarker.getPosition().longitude!=myUser.getLon())){
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLastMassageCoord,14));
                myUserLastMassageMarker.remove();
            }
            if (myUser.getMsg() != null) {
                myUserLastMassageMarker = map.addMarker(new MarkerOptions().position(myLastMassageCoord).title(myUser.getUsername()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).zIndex(1000));
                myUserLastMassageMarker.setSnippet(myUser.getMsg());
            }
        };
        userViewModel.getMyUser().observe(this, observeMyUser);
        moveToMyPosition();
    }

    public void moveToMyPosition(){
        if(myUserMarker!=null)
            myUserMarker.remove();
        MyUser myUser = DbManager.getDatabase(getActivity().getApplicationContext()).myUserModel().getMyUserNoLive();
        LatLng myPosition = new LatLng(myUser.getLat(), myUser.getLon());
        myUserMarker = map.addMarker(new MarkerOptions().position(myPosition).title(myUser.getUsername()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).zIndex(999));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition,14));
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
