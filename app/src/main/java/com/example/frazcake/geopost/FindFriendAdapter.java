package com.example.frazcake.geopost;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.Response;
import com.example.frazcake.geopost.api.UsersListRequestSender;
import com.example.frazcake.geopost.model.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FindFriendAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<User> resultList = new ArrayList<>();
    private Response.Listener<JSONObject> respList;

    public FindFriendAdapter(Context context, Response.Listener<JSONObject> respList) {
        mContext = context;
        this.respList = respList;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public User getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.find_friends_list, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getUsername());
        ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getMsg());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<String> strings = new ArrayList<>();
                    UsersListRequestSender.send(mContext, constraint.toString(), respList);
                    // Assign the data to the FilterResults
                    filterResults.values = strings;
                    filterResults.count = strings.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<User>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}
