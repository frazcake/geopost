package com.example.frazcake.geopost.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public abstract class ProfileStatus {

    private static String mtoken;
    public static void send(final Context ctx, String token) {
        mtoken=token;
        String url = ApiUtils.base_url + "profile?session_id=" + token;
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null, onResponse(ctx), onError(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Response.Listener<JSONObject> onResponse(final Context ctx){
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gsonParser = new Gson();
                try {
                    MyUser myUser = new MyUser(response.getString("username"), mtoken);
                    double lat = response.getDouble("lat");
                    double lon = response.getDouble("lon");
                    myUser.setMsg(response.getString("msg"));
                    myUser.setUsername(response.getString("username"));
                    myUser.setMsglat(lat);
                    myUser.setLat(lat);
                    myUser.setLon(lon);
                    myUser.setMsglon(lon);
                    DbManager.getDatabase(ctx).myUserModel().insertMyUser(myUser);
                    GetVipRequest.send(ctx, mtoken);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private static Response.ErrorListener onError(final Context ctx){
        return error -> {
            if (error.networkResponse !=null) {
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(ctx, body, Toast.LENGTH_SHORT).show();
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(ctx, "Network error! Check your internet connection and retry", Toast.LENGTH_SHORT).show();
        };
    }
}