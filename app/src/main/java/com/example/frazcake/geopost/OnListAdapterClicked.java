package com.example.frazcake.geopost;

import com.example.frazcake.geopost.model.FollowedUser;

public interface OnListAdapterClicked {
    void onClick(FollowedUser followedUser);
}
