package com.example.frazcake.geopost.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.frazcake.geopost.model.DbManager;

public class MainEmptyActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent activityIntent;
        String token = DbManager.getDatabase(getApplicationContext()).myUserModel().getToken();
        if (isTokenValid(token)) {
            activityIntent = new Intent(this, MainActivity.class);
        } else {
            DbManager.getDatabase(getApplicationContext()).clearAllTables();
            activityIntent = new Intent(this, LoginActivity.class);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        }
        startActivity(activityIntent);
        finish();
    }

    private boolean isTokenValid(String token){
        if (token != null) {
            return true;
        } else {
            return false;
        }
    }
}