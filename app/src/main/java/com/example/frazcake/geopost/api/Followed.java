package com.example.frazcake.geopost.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.FollowedUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public abstract class Followed {

    public static void send(final Context ctx) {
        String url = ApiUtils.base_url + "followed?session_id=" + ApiUtils.getToken(ctx);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null, onResponse(ctx), onError(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Response.Listener<JSONObject> onResponse(final Context ctx){
        return response -> {
            Gson gsonParser = new Gson();
            Type userListType = new TypeToken<Collection<FollowedUser>>(){}.getType();
            try {
                List<FollowedUser> followedUsers = gsonParser.fromJson(response.getJSONArray("followed").toString(), userListType);
                DbManager.getDatabase(ctx).followedUserModel().insertUsers(followedUsers);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };
    }

    private static Response.ErrorListener onError(final Context ctx){
        return error -> {
            if (error.networkResponse !=null) {
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(ctx, body, Toast.LENGTH_SHORT).show();
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(ctx, "Network error! Check your internet connection and retry", Toast.LENGTH_SHORT).show();
        };
    }
}