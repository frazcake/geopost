package com.example.frazcake.geopost;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.frazcake.geopost.model.FollowedUser;
import com.example.frazcake.geopost.model.MyUser;
import com.example.frazcake.geopost.model.Repository;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    private Repository repository;
    public UserViewModel(@NonNull Application application) {
        super(application);
        this.repository = new Repository(application);
    }

    public LiveData<List<FollowedUser>> getUsers() {
        return repository.loadAllUsers();
    }

    public LiveData<MyUser> getMyUser() {
        return repository.getMyUser();
    }

    public void insertMyUser(MyUser u){
        repository.insertMyUser(u);
    }

    public void insertUsers(List<FollowedUser> followedUsers){
        repository.insertUsers(followedUsers);
    }

}
