package com.example.frazcake.geopost.model;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;
import com.example.frazcake.geopost.model.FollowedUser;

import java.util.List;

public class Repository {

    private DbManager db;
    public Repository(Application application){
        this.db = DbManager.getDatabase(application.getApplicationContext());
    }

    public LiveData<List<FollowedUser>> loadAllUsers() {
        return db.followedUserModel().loadAllUsers();
    }

    public LiveData<MyUser> getMyUser() {
        return db.myUserModel().getMyUser();
    }

    public void insertMyUser(MyUser u) {
        db.myUserModel().insertMyUser(u);
    }

    public void insertUsers(List<FollowedUser> followedUsers) {
        db.followedUserModel().insertUsers(followedUsers);
    }
}
