package com.example.frazcake.geopost.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;

import java.io.UnsupportedEncodingException;

public abstract class GetVipRequest {

    private static String mtoken;
    public static void send(final Context ctx, String token) {
        mtoken=token;
        String url = ApiUtils.base_url + "getVip?session_id=" + token;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, onResponse(ctx), onError(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Response.Listener<String> onResponse(final Context ctx){
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MyUser myUser = DbManager.getDatabase(ctx).myUserModel().getMyUserNoLive();
                if(response.equals("true"))
                    myUser.setVip(true);
                else
                    myUser.setVip(false);
                DbManager.getDatabase(ctx).myUserModel().insertMyUser(myUser);
                Log.e("HttpClient", "preso correttamente parametro vip " + response);

            }
        };
    }

    private static Response.ErrorListener onError(final Context ctx){
        return error -> {
            Toast.makeText(ctx.getApplicationContext(), "Errore Vip", Toast.LENGTH_LONG).show();
            if (error.networkResponse !=null) {
                Log.e("HttpClient", "errore get vip");
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(ctx, body, Toast.LENGTH_SHORT).show();
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(ctx, "Network error! Check your internet connection and retry", Toast.LENGTH_SHORT).show();
        };
    }
}