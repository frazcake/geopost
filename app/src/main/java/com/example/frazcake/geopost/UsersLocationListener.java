package com.example.frazcake.geopost;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;

public class UsersLocationListener implements LocationListener {

    Context ctx;
    public UsersLocationListener(Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    @Override
    public void onLocationChanged(Location location) {
        MyUser myUser = DbManager.getDatabase(ctx).myUserModel().getMyUserNoLive();
        if(myUser!=null) {
            myUser.setLat(location.getLatitude());
            myUser.setLon(location.getLongitude());
            DbManager.getDatabase(ctx).myUserModel().insertMyUser(myUser);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
