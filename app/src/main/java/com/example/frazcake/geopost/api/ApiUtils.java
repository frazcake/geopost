package com.example.frazcake.geopost.api;

import android.content.Context;

import com.example.frazcake.geopost.model.DbManager;

public abstract class ApiUtils {
    
    public static final String base_url = "https://ewserver.di.unimi.it/mobicomp/geopost/";

    public static String getToken(Context ctx){
        return DbManager.getDatabase(ctx.getApplicationContext()).myUserModel().getToken();
    }
}
