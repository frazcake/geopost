package com.example.frazcake.geopost.UI;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyUserProfileMapsFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap map;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        if(savedInstanceState==null) {
            SupportMapFragment mapFrag = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.myuser_map));
            mapFrag.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map.getUiSettings().setMapToolbarEnabled(false);
        final Observer<MyUser> observerUser = new Observer<MyUser>() {
            @Override
            public void onChanged(@Nullable MyUser myUser) {
                map.clear();
                if(myUser == null)
                    return;
                LatLng coord = new LatLng(myUser.getMsglat(), myUser.getMsglon());
                Marker m = map.addMarker(new MarkerOptions().position(coord).title(myUser.getUsername()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).zIndex(1000));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(coord, 12.0f));
                m.setSnippet(myUser.getMsg());
                m.setVisible(true);
                m.showInfoWindow();
            }
        };
        DbManager.getDatabase(getActivity().getApplicationContext()).myUserModel().getMyUser().observe(getActivity(), observerUser);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

}
