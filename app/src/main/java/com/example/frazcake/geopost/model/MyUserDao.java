package com.example.frazcake.geopost.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface MyUserDao {

    @Insert(onConflict = REPLACE)
    void insertMyUser(MyUser user);

    @Query("DELETE FROM MyUser")
    void deleteMyUser();

    @Query("SELECT * FROM MyUser")
    LiveData<MyUser> getMyUser();

    @Query("SELECT * FROM MyUser")
    MyUser getMyUserNoLive();

    @Query("SELECT token FROM MyUser")
    String getToken();
}