package com.example.frazcake.geopost.UI;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.example.frazcake.geopost.FindFriendAdapter;
import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.api.Follow;
import com.example.frazcake.geopost.api.UsersListRequestSender;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FindFriendActivity extends AppCompatActivity implements TextWatcher {

    private List<String> users = new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        AutoCompleteTextView usernameAutocompleteTextView = findViewById(R.id.find_friends_actw);
        usernameAutocompleteTextView.addTextChangedListener(this);
        usernameAutocompleteTextView.setThreshold(3);
        Response.Listener<JSONObject> respList = null;
        usernameAutocompleteTextView.setAdapter(new FindFriendAdapter(getApplicationContext(), respList)); // 'this' is Activity instance
        usernameAutocompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String username = adapterView.getItemAtPosition(position).toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(FindFriendActivity.this);
                // Add the buttons
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        Follow.send(getApplicationContext(), username);
                        Toast.makeText(getApplicationContext(), username + "aggiunto correttamente",Toast.LENGTH_SHORT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                builder.setMessage("Vuoi aggiungere " + username + "tra gli amici?");
                builder.setTitle("Add Friend");
                final AlertDialog dialog = builder.create();
                dialog.show();
                //usernameAutocompleteTextView.setText(username);
            }
        });

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Response.Listener<JSONObject> respList = null;
        respList = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gsonParser = new Gson();
                Type userListType = new TypeToken<Collection<String>>() {
                }.getType();
                try {
                    users.clear();
                    users.addAll(gsonParser.fromJson(response.getJSONArray("usernames").toString(), userListType));
                    adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, users);
                    AutoCompleteTextView usernameAutocompleteTextView = findViewById(R.id.find_friends_actw);
                    usernameAutocompleteTextView.setThreshold(3);
                    usernameAutocompleteTextView.setAdapter(adapter); // 'this' is Activity instance
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String userStart = ((TextView) findViewById(R.id.find_friends_actw)).getText().toString();
        UsersListRequestSender.send(getApplicationContext(), userStart, respList);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
