package com.example.frazcake.geopost;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.frazcake.geopost.model.FollowedUser;

import java.text.DecimalFormat;

public class UserListAdapter extends ListAdapter<FollowedUser, UserViewHolder> {

    private final OnListAdapterClicked onListAdapterClicked;

    public UserListAdapter(OnListAdapterClicked onListAdapterClicked) {
        super(FollowedUser.DIFF_CALLBACK);
        this.onListAdapterClicked = onListAdapterClicked;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        UserViewHolder userViewHolder = new UserViewHolder(itemView);
        itemView.setOnClickListener(new MyOnClickListener(userViewHolder));
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
            FollowedUser current = getItem(position);
            holder.name.setText(current.getUsername());
            holder.msg.setText(current.getMsg());
            if(current.isVip())
                holder.imageView.setVisibility(View.VISIBLE);
            String distance="";
            DecimalFormat twoDForm = new DecimalFormat("#.##");
            float currDistance = current.getDistance();
            if(currDistance<1000)
                distance=""+currDistance+" m";
            else
                distance=""+Float.valueOf(twoDForm.format(currDistance/1000))+" Km";
            holder.distance.setText(distance);  //\nposition " + current.getLat() + ", " + current.getLon());
    }


    private class MyOnClickListener implements View.OnClickListener {
        private UserViewHolder userViewHolder;
        MyOnClickListener(UserViewHolder userViewHolder) {
            this.userViewHolder = userViewHolder;
        }

        @Override
        public void onClick(final View view) {
            onListAdapterClicked.onClick(getItem(userViewHolder.getAdapterPosition()));
        }
    }
}