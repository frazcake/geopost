package com.example.frazcake.geopost.UI;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.frazcake.geopost.OnListAdapterClicked;
import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.UserListAdapter;
import com.example.frazcake.geopost.UserViewModel;
import com.example.frazcake.geopost.api.Followed;
import com.example.frazcake.geopost.model.FollowedUser;
import com.example.frazcake.geopost.model.MyUser;

import java.util.List;

public class FollowedFragment extends Fragment {

    private int mCurCheckPosition = 0;
    private UserListAdapter adapter;
    private RecyclerView recyclerView;
    private UserViewModel userViewModel;
    private UserMapsFragment userMapsFragment;
    private MyUser mu;
    private OnListAdapterClicked onListAdapterClicked = new OnListAdapterClicked() {
        @Override
        public void onClick(FollowedUser followedUser) {
            userMapsFragment.updateMap(followedUser);
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        Followed.send(getActivity().getApplicationContext());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userMapsFragment = (UserMapsFragment) getFragmentManager().findFragmentById(R.id.map);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView = getActivity().findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UserListAdapter(onListAdapterClicked);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);
        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        userViewModel.getMyUser().observe(this, new Observer<MyUser>() {
            @Override
            public void onChanged(@Nullable MyUser myUser) {
                mu = myUser;
                userViewModel.getUsers().observe(getActivity(), new Observer<List<FollowedUser>>() {
                    @Override
                    public void onChanged(@Nullable List<FollowedUser> users) {
                        float[] results = new float[1];
                        for (FollowedUser fu: users) {
                            Location.distanceBetween(mu.getLat(),mu.getLon(),fu.getLat(),fu.getLon(),results);
                            fu.setDistance(results[0]);
                        }
                        users.sort((o1, o2) -> {
                            if(o1.isVip() && o2.isVip()) {
                                if (o1.getDistance() > o2.getDistance()) {
                                    return 1;
                                } else if (o1.getDistance() < o2.getDistance()) {
                                    return -1;
                                } else {
                                    return 0;
                                }
                            }else if(o1.isVip())
                                return -1;
                            else if(o2.isVip())
                                return 1;
                            else {
                                if (o1.getDistance() > o2.getDistance()) {
                                    return 1;
                                } else if (o1.getDistance() < o2.getDistance()) {
                                    return -1;
                                } else {
                                    return 0;
                                }
                            }
                        });
                        adapter.submitList(users);
                    }
                });
            }
        });

        /*if (savedInstanceState == null) {
              //Followed.send(getActivity().getApplicationContext());
        }*/
    }



    //Devi aggiungere la query per settare la distanza nel db user, creare l'user corrente dalle preferences a runtime
    //e finire il listener richiamando il metodo setDistance ogni volta che cambia la posizione, e anche il metodo setLatLon
    //sul mio utente.




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.followed_fragment, container, false);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", mCurCheckPosition);
    }
}
