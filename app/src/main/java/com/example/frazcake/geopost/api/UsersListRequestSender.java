package com.example.frazcake.geopost.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class UsersListRequestSender {

    public static void send(final Context ctx, String usernameStart, Response.Listener<JSONObject> respList) {
        String url = ApiUtils.base_url + "users?session_id=" + ApiUtils.getToken(ctx) + "&usernamestart=" +
                usernameStart; //manca ancora limit
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null, respList, onError(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Response.ErrorListener onError(final Context ctx){
        return error -> {
            if (error.networkResponse !=null) {
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(ctx, body, Toast.LENGTH_SHORT).show();
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(ctx, "Network error! Check your internet connection and retry", Toast.LENGTH_SHORT).show();
        };
    }
}
