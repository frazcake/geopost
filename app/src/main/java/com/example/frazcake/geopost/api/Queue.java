package com.example.frazcake.geopost.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Queue {
    private static Queue singletonInstance;
    private RequestQueue requestQueue;

    private Queue(Context ctx) {
        requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
    }

    public static synchronized Queue getInstance(Context context) {
        if (singletonInstance == null) {
            singletonInstance = new Queue(context);
        }
        return singletonInstance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        requestQueue.add(req);
    }
}