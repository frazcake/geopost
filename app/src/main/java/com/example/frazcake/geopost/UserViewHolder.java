package com.example.frazcake.geopost;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class UserViewHolder extends RecyclerView.ViewHolder {
    public final View container;
    public final ImageView imageView;
    public final TextView name;
    public final TextView msg;
    public final TextView distance;

    public UserViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.image);
        imageView.setVisibility(View.GONE);
        name = itemView.findViewById(R.id.name);
        msg = itemView.findViewById(R.id.msg);
        container = itemView.findViewById(R.id.recycle_view_item);
        distance = itemView.findViewById(R.id.distance);
    }
}
