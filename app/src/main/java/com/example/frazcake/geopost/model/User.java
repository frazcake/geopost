package com.example.frazcake.geopost.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity()
public class User {

    @PrimaryKey
    @NonNull
    protected String username;

    protected double lat=0.0;
    protected double lon=0.0;
    protected String msg;
    protected boolean vip;

    public User(@NonNull String username) {
        this.username = username;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    @NonNull
    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return String.format("%s (msg = %s)", username, msg);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object u){
        if(u == null)
            return false;
        if (getClass() != u.getClass())
            return false;
        User user = (User) u;
        if(user.username==username && user.msg==msg && user.lat==lat && user.lon==lon)
            return true;
        return false;
    }
}
