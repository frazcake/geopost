package com.example.frazcake.geopost.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.api.Login;
import com.example.frazcake.geopost.model.DbManager;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        DbManager.getDatabase(getApplicationContext()).clearAllTables();
        usernameEditText = findViewById(R.id.editTextUserName);
        passwordEditText = findViewById(R.id.editTextPassword);
    }

    public void loginRoutine(View v){
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if(isUsernameValid(username) && isPasswordValid(password)){
            Login.send(getApplicationContext(), username, password);
        }
    }

    private boolean isPasswordValid(String password) {
        if(password.length()<4){
            passwordEditText.setError("Password must have at least 8 characters");
            return false;
        }
        return true;
    }

    private boolean isUsernameValid(String username) {
        if(username.isEmpty()) {
            usernameEditText.setError("Empty Username");
            return false;
        }
        return true;
    }

}