package com.example.frazcake.geopost;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.frazcake.geopost.api.ApiUtils;
import com.example.frazcake.geopost.api.Queue;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;

import java.io.UnsupportedEncodingException;

public abstract class SetVipRequest {
    private static String mVip;

    public static void send(final Context ctx, String vip) {
        mVip=vip;
        String token = DbManager.getDatabase(ctx.getApplicationContext()).myUserModel().getToken();
        String url = ApiUtils.base_url + "setVip?session_id=" + token + "&vip=" + vip;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseRoutine(ctx), onError(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Response.Listener responseRoutine(final Context ctx){
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MyUser myUser = DbManager.getDatabase(ctx.getApplicationContext()).myUserModel().getMyUserNoLive();
                if(mVip=="true")
                    myUser.setVip(true);
                else if(mVip=="false")
                    myUser.setVip(false);
                DbManager.getDatabase(ctx.getApplicationContext()).myUserModel().insertMyUser(myUser);
                Log.e("HttpClient", "settato vip o non vip correttamente " + response);
            }
        };
        return responseListener;
    }

    private static Response.ErrorListener onError(final Context ctx){
        return error -> {
            if (error.networkResponse !=null) {
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    Toast.makeText(ctx, body, Toast.LENGTH_SHORT).show();
                } catch(UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else
                Toast.makeText(ctx, "Network error! Check your internet connection and retry", Toast.LENGTH_SHORT).show();
        };
    }
}