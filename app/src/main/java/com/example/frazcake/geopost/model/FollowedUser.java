package com.example.frazcake.geopost.model;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

@Entity()
public class FollowedUser extends User {
    public static DiffUtil.ItemCallback<FollowedUser> DIFF_CALLBACK = new DiffUtil.ItemCallback<FollowedUser>() {
        @Override
        public boolean areItemsTheSame(@NonNull FollowedUser oldItem, @NonNull FollowedUser newItem) {
            //questo metodo se ritorna true fa chiamare areContentsTheSame per vedere se si è aggiornato l'oggetto
            return oldItem.username.equals(newItem.username);
        }

        @Override
        public boolean areContentsTheSame(@NonNull FollowedUser oldItem, @NonNull FollowedUser newItem) {
            return oldItem.equals(newItem);
        }
    };

    private float distance;

    public FollowedUser(@NonNull String username) {
        super(username);
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getDistance() {
        return distance;
    }

}
