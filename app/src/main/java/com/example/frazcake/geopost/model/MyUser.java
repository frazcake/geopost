package com.example.frazcake.geopost.model;

import android.arch.persistence.room.Entity;

@Entity()
public class MyUser extends User {
    private String token;
    private double msglat=0.0;
    private double msglon=0.0;

    public MyUser(String username, String token) {
        super(username);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public double getMsglon() {
        return msglon;
    }

    public double getMsglat() {
        return msglat;
    }

    public void setMsglat(double msglat) {
        this.msglat = msglat;
    }

    public void setMsglon(double msglon) {
        this.msglon = msglon;
    }
}
