package com.example.frazcake.geopost.api;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.example.frazcake.geopost.UI.MainActivity;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;

import java.util.HashMap;
import java.util.Map;

public abstract class Login{

    public static void send(final Context ctx, final String username, final String password){
        String url = ApiUtils.base_url + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, responseRoutine(ctx, username), errorRoutine(ctx)){
            @Override
            protected Map<String,String> getParams(){return addParams(username,password);}
        };
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }

    private static Map<String,String> addParams(final String username, final String password){
        Map<String,String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        return params;
    }

    private static Response.Listener<String> responseRoutine(final Context ctx, final String username){
        return response -> {
            Toast.makeText(ctx, "Login success", Toast.LENGTH_SHORT).show();
            MyUser myUser = new MyUser(username, response);
            DbManager.getDatabase(ctx).myUserModel().insertMyUser(myUser);
            ProfileStatus.send(ctx, response);
            ctx.startActivity(new Intent(ctx, MainActivity.class));
        };
    }

    private static Response.ErrorListener errorRoutine(final Context ctx){
        return error -> Toast.makeText(ctx, "Login failed, retry", Toast.LENGTH_LONG).show();
    }
}