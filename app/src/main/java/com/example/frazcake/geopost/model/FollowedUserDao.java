package com.example.frazcake.geopost.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface FollowedUserDao {

    @Insert(onConflict = REPLACE)
    void insertFollowedUser(FollowedUser followedUser);

    @Insert(onConflict = REPLACE)
    void insertUsers(List<FollowedUser> followedUsers);

    @Query("SELECT * FROM FollowedUser ORDER BY distance ASC")
    LiveData<List<FollowedUser>> loadAllUsers();

    @Query("SELECT * FROM FollowedUser WHERE username = :usr")
    LiveData<FollowedUser> getFollowedUser(String usr);

    @Query("DELETE FROM FollowedUser")
    void deleteAll();

    @Delete
    void deleteFollowedUser(FollowedUser n);

}