package com.example.frazcake.geopost.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {FollowedUser.class, MyUser.class}, version = 1)
public abstract class DbManager extends RoomDatabase {

    private static DbManager INSTANCE;

    public abstract FollowedUserDao followedUserModel();
    public abstract MyUserDao myUserModel();

    public static DbManager getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.inMemoryDatabaseBuilder(context.getApplicationContext(), DbManager.class)
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static DbManager getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), DbManager.class, "geopost_db")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}