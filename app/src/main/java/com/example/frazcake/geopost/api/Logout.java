package com.example.frazcake.geopost.api;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.frazcake.geopost.model.DbManager;

public abstract class Logout {

    public static void send(final Context ctx, Response.Listener<String> resplist) {
        String token = DbManager.getDatabase(ctx.getApplicationContext()).myUserModel().getToken();
        String url = ApiUtils.base_url + "logout?session_id=" + token;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, resplist, errorRoutine(ctx));
        Queue.getInstance(ctx).addToRequestQueue(stringRequest);
    }
    private static Response.ErrorListener errorRoutine(final Context ctx){
        Response.ErrorListener errListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ctx, "Errore di rete, logout non riuscito", Toast.LENGTH_SHORT);
            }
        };
        return errListener;
    }
}
