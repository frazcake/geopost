package com.example.frazcake.geopost.UI;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.example.frazcake.geopost.R;
import com.example.frazcake.geopost.SetVipRequest;
import com.example.frazcake.geopost.api.Logout;
import com.example.frazcake.geopost.model.DbManager;
import com.example.frazcake.geopost.model.MyUser;

import java.text.DecimalFormat;

public class ProfileActivity extends AppCompatActivity {
    private String vipRequestParameter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.buttonLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout.send(getApplicationContext(),responseListener);
            }
        });
        DbManager.getDatabase(getApplicationContext()).myUserModel().getMyUser().observe(this, new Observer<MyUser>() {
            @Override
            public void onChanged(@Nullable MyUser myUser) {
                if(myUser == null)
                    return;
                TextView myusernametv = findViewById(R.id.username_profile);
                myusernametv.setText(myUser.getUsername());
                ((TextView) findViewById(R.id.message_profile)).setText(myUser.getMsg());
                DecimalFormat fourDForm = new DecimalFormat("#.####");
                float fourDecLat= Float.valueOf(fourDForm.format(myUser.getMsglat()));
                float fourDecLon = Float.valueOf(fourDForm.format(myUser.getMsglon()));
                ((TextView) findViewById(R.id.position_profile)).setText(fourDecLat + ", " + fourDecLon);
                Button vipbutton = findViewById(R.id.vip_button);
                vipbutton.setOnClickListener(vipListener);
                if(myUser.isVip()) {
                    ((TextView) findViewById(R.id.vipSiNo)).setText("Sei un vip");
                    vipbutton.setText("torna utente normale");
                    vipRequestParameter="false";
                } else if(!myUser.isVip()){
                    ((TextView) findViewById(R.id.vipSiNo)).setText("non sei un utente VIP");
                    vipbutton.setText("diventa VIP");
                    vipRequestParameter="true";
                }

            }
        });
    }

    private View.OnClickListener vipListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SetVipRequest.send(getApplicationContext(), vipRequestParameter);
        }
    };

    Response.Listener<String> responseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.e("HttpClient", "success! response: " + response);
            Intent intent = new Intent(ProfileActivity.this.getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ProfileActivity.this.startActivity(intent);
            ProfileActivity.this.finish();
        }
    };
}
